use std::{io::{Write, stdin, stdout, Stdout}, rc::Rc, fmt::Display};
use crate::{
    eval::{eval, Env, Error as EvalError, stdlib},
    parse::{lex::TokenBuf, Parser, Error as ParseError},
};

const PROMPT: &str = "> ";

enum Error {
    Parse(ParseError),
    Eval(EvalError),
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Parse(err) => write!(f, "Parse error: {}", err),
            Error::Eval(err) => write!(f, "Eval error: {}", err),
        }
    }
}

impl From<ParseError> for Error { fn from(err: ParseError) -> Self { Self::Parse(err) } }
impl From<EvalError>  for Error { fn from(err: EvalError ) -> Self { Self::Eval(err ) } }

pub fn run() {
    let stdin = stdin();
    let mut stdout = stdout();

    let top_env = stdlib();
    let mut parser = Parser::default();

    write!(stdout, "{}", PROMPT).unwrap();
    stdout.flush().unwrap();

    let mut buf = String::with_capacity(128);

    while stdin.read_line(&mut buf).unwrap() > 0 {
        if let Some(tokens) = TokenBuf::lex(&buf) {
            parser.load(tokens);

            if let Err(err) = eval_available(&mut parser, &top_env, &mut stdout) {
                eprintln!("{}", err);
                parser.clear();
            }

            write!(stdout, "{}", PROMPT).unwrap();
            stdout.flush().unwrap();
        };

        buf.clear();
    }
}

fn eval_available(parser: &mut Parser, env: &Rc<Env>, output: &mut Stdout) -> Result<(), Error> {
    for expr in parser {
        let result = eval(env, &expr?)?;
        writeln!(output, "{}", result).unwrap();
    }

    Ok(())
}
