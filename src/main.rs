#![feature(
    bool_to_option,
    stdio_locked,
    array_chunks,
    assert_matches,
    let_else,
    explicit_generic_args_with_impl_trait,
)]

mod repr;
mod parse;
mod eval;
mod repl;

fn main() {
    repl::run();
}
