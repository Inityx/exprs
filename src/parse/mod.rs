pub mod lex;

use lex::{TokenBuf, QUOTE};
use crate::repr::{Expr, types::{Cons, Number, Quoted, Stryng, Symbol}};

#[derive(Clone, Debug, PartialEq)]
pub struct Error(pub String);

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

impl Error {
    pub fn expected(s: &str, got: &str) -> Self {
        Self(format!("Expected {}, got {}", s, got))
    }
}

#[derive(Default)]
pub struct Parser(TokenBuf);

impl Parser {
    pub fn parse(source: &str) -> Result<Vec<Expr>, Error> {
        let Some(tokens) = TokenBuf::lex(source) else {
            return Err(Error("Expected end of string literal".to_string()));
        };
        Self(tokens).collect()
    }

    pub fn load(&mut self, tokens: TokenBuf) {
        self.0.extend(tokens);
    }

    pub fn clear(&mut self) {
        self.0 = TokenBuf::default();
    }

    fn expr(&mut self) -> Option<Result<Expr, Error>> {
        let next_token = self.0.peek()?;

        Some(match next_token {
            "(" => self.list()?,
            ")" => {
                self.0.next(); // consume )
                Err(Error::expected("an expression", ")"))
            }
            "'" => {
                self.0.next(); // consume '
                self.expr()?.map(Quoted::new).map(Expr::Quoted)
            }
            "nil" => {
                self.0.next(); // consume nil
                Ok(Expr::Nil)
            }
            token => {
                Ok(if token.starts_with(QUOTE) {
                    self.string()
                } else {
                    let token = self.0.next().unwrap();

                    str::parse::<Number>(token).map(Expr::Number)
                        .unwrap_or_else(|_| Expr::from(Symbol::from(token.to_string())))
                })
            }
        })
    }

    fn list(&mut self) -> Option<Result<Expr, Error>> {
        let opener = self.0.next().unwrap();
        debug_assert_eq!(opener, "(");

        self.list_contents()
    }

    fn list_contents(&mut self) -> Option<Result<Expr, Error>> {
        Some(match self.0.peek()? {
            ")" => {
                self.0.next(); // Consume `)`
                Ok(Expr::Nil)
            }
            _ => {
                let head = match self.expr() {
                    Some(Ok(head)) => head,
                    fail => return fail,
                };
                let tail = match self.list_contents() {
                    Some(Ok(tail)) => tail,
                    fail => return fail,
                };
                Ok(Expr::Cons(Cons::new(head, tail)))
            }
        })
    }

    fn string(&mut self) -> Expr {
        let string = self.0.next().unwrap();
        debug_assert!(string.starts_with('"'));
        debug_assert!(string.ends_with('"'));

        Expr::from(Stryng::from(string
            .strip_prefix(QUOTE).unwrap()
            .strip_suffix(QUOTE).unwrap()
            .replace("\\\"", "\"")
        ))
    }
}

impl Iterator for &mut Parser {
    type Item = Result<Expr, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.rewind();
        self.0.peek()?;
        let expr = self.expr();
        if expr.is_some() { self.0.cut(); }
        expr
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_basic() {
        assert_eq!(
            Parser::parse("(+ 2 3)"),
            Ok(vec![Expr::new_list([
                Expr::from(Symbol::from("+")),
                Expr::Number(2.0),
                Expr::Number(3.0),
            ])]),
        );
    }

    #[test]
    fn parse_complex() {
        assert_eq!(
            Parser::parse("(sqrt (+ 2 3))"),
            Ok(vec![Expr::new_list([
                Symbol::from("sqrt").into(),
                Expr::new_list([
                    Symbol::from("+").into(),
                    Expr::Number(2.0),
                    Expr::Number(3.0),
                ]),
            ])]),
        );
    }

    #[test]
    fn parse_quoted() {
        assert_eq!(
            Parser::parse("(atom? '(+ 2 3))"),
            Ok(vec![Expr::new_list([
                Symbol::from("atom?").into(),
                Quoted::new(Expr::new_list([
                    Symbol::from("+").into(),
                    Expr::Number(2.0),
                    Expr::Number(3.0),
                ])).into(),
            ])]),
        );
    }

    #[test]
    fn parse_escaped_string() {
        assert_eq!(
            Parser::parse(r#"("foo bar" "bar()\"")"#),
            Ok(vec![Expr::new_list([
                Stryng::from(r#"foo bar"#).into(),
                Stryng::from(r#"bar()""#).into(),
            ])]),
        );
    }

    #[test]
    fn parse_partial() {
        let mut parser = Parser::default();
        parser.load(TokenBuf::lex("(").unwrap());
        assert_eq!( (&mut parser).next(), None);

        parser.load(TokenBuf::lex("foo 1.4").unwrap());
        assert_eq!(
            (&mut parser).next(),
            None,
            "Parser should continue the previous parse\nCurrent buffer: {:#?}", parser.0,
        );

        parser.load(TokenBuf::lex(")").unwrap());
        assert_eq!(
            (&mut parser).next(),
            Some(Ok(Expr::new_list([
                Symbol::from("foo").into(),
                Expr::Number(1.4),
            ])))
        );
        assert_eq!((&mut parser).next(), None, "Parser should not re-parse the same token");

        parser.load(TokenBuf::lex("(bar)").unwrap());
        assert_eq!(
            (&mut parser).next(),
            Some(Ok(Expr::new_list([Symbol::from("bar").into()])))
        );
    }

    #[test]
    fn parse_failures() {
        assert_eq!(
            Parser::parse("())"),
            Err(Error::expected("an expression", ")")),
        );
        assert_eq!(
            Parser::parse("(+ 4 5))"),
            Err(Error::expected("an expression", ")")),
        );
    }
}
