#[derive(Clone, Copy, PartialEq, Debug)]
struct SubStr { start: usize, len: usize }

impl SubStr {
    fn slice<'str>(&'_ self, s: &'str str) -> &'str str {
        &s[self.start..][..self.len]
    }

    fn subtracting(mut self, x: usize) -> Self {
        self.start = self.start.checked_sub(x).expect("Tried to move SubStr negative");
        self
    }

    fn adding(mut self, x: usize) -> Self {
        self.start += x;
        self
    }
}

pub const SEPARATOR: &str = "()'\"";
pub const QUOTE: char = '"';
pub const ESCAPE: char = '\\';

fn separator(c: char) -> bool {
    SEPARATOR.contains(c)
}

fn non_identifier(c: char) -> bool {
    c.is_whitespace() || separator(c) || c == QUOTE
}

fn end_of_token(string: &str) -> Option<usize> {
    if string.starts_with(QUOTE) {
        string.char_indices().skip(1).find_map(|(i, curr)| {
            let prev = string[i - 1..].chars().next().unwrap();
            match (prev, curr) {
                (ESCAPE, QUOTE) => None,
                (_, QUOTE) => Some(i + 1),
                _ => None,
            }
        })
    } else if string.starts_with(separator) {
        Some(1) // All separators are a single character
    } else {
        Some(string.find(non_identifier).unwrap_or(string.len()))
    }
}

#[derive(Debug, PartialEq, Clone, Default)]
pub struct TokenBuf {
    code: String,
    lexemes: Vec<SubStr>,
    i: usize,
}

impl TokenBuf {
    pub fn lex(code: impl Into<String>) -> Option<TokenBuf> {
        let code = code.into();

        let mut lexemes = Vec::new();
        let mut token_start = 0;
        loop {
            token_start += match &code[token_start..].find(|c: char| !c.is_whitespace()) {
                Some(i) => i,
                None => break,
            };
            let remaining = &code[token_start..];
            let token_len = end_of_token(remaining)?;

            lexemes.push(SubStr {
                start: token_start,
                len: token_len,
            });

            token_start += token_len;
        }

        Some(Self { code, lexemes, i: 0 })
    }

    pub(super) fn extend(&mut self, other: Self) {
        let prev_end = self.code.len();
        self.code += &other.code;
        self.lexemes.extend(other.lexemes.into_iter().map(|substr| substr.adding(prev_end)));
    }

    pub(super) fn cut(&mut self) {
        let Some(cut_point) = self.lexemes.get(self.i).map(|next| next.start) else {
            return *self = Self::default()
        };

        self.code = self.code.split_off(cut_point);
        self.lexemes = ::std::mem::take(&mut self.lexemes).into_iter()
            .skip(self.i)
            .map(|substr| substr.subtracting(cut_point))
            .collect();
        self.i = 0;
    }

    pub(super) fn rewind(&mut self) {
        self.i = 0;
    }

    pub(super) fn peek(&self) -> Option<&str> {
        self.lexemes.get(self.i).map(|substr| substr.slice(&self.code))
    }

    pub(super) fn next(&mut self) -> Option<&str> {
        let Self { ref lexemes, ref mut i, ref code } = self;

        lexemes.get(*i).map(|substr| {
            *i += 1;
            substr.slice(code.as_str())
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn s(start: usize, len: usize) -> SubStr {
        SubStr { start, len }
    }

    fn to_vec_string(mut tokens: TokenBuf) -> Vec<String> {
        let mut vec = Vec::with_capacity(tokens.lexemes.len());

        while let Some(token) = tokens.next() {
            vec.push(token.to_string());
        }

        vec
    }

    #[test]
    fn lex_basic() {
        assert_eq!(
            to_vec_string(TokenBuf::lex(" (+ 2(+ 3 4)5)").unwrap()),
            ["(", "+", "2", "(", "+", "3", "4", ")", "5", ")"],
        );
    }

    #[test]
    fn lex_complex() {
        assert_eq!(
            to_vec_string(TokenBuf::lex("(sqrt (+ 2 3))").unwrap()),
            ["(", "sqrt", "(", "+", "2", "3", ")", ")"],
        );
    }

    #[test]
    fn lex_quoted() {
        assert_eq!(
            to_vec_string(TokenBuf::lex("(atom? '(+ 2 3))").unwrap()),
            ["(", "atom?", "'", "(", "+", "2", "3", ")", ")"],
        );
    }

    #[test]
    fn lex_escaped_strings() {
        assert_eq!(
            to_vec_string(TokenBuf::lex(r#"("foo bar" "bar()\"")"#).unwrap()),
            ["(", r#""foo bar""#, r#""bar()\"""#, ")"],
        );
    }

    #[test]
    fn token_buf_extend_cut() {
        let mut buf = TokenBuf::default();
        assert_eq!(buf.code, "");
        assert_eq!(buf.lexemes, []);
        assert_eq!(buf.i, 0);

        buf.extend(TokenBuf::lex("hello world").unwrap());
        assert_eq!(buf.code, "hello world");
        assert_eq!(buf.lexemes, [s(0, 5), s(6, 5)]);
        assert_eq!(buf.i, 0);

        buf.extend(TokenBuf::lex("foo").unwrap());
        assert_eq!(buf.code, "hello worldfoo");
        assert_eq!(buf.lexemes, [s(0, 5), s(6, 5), s(11, 3)]);
        assert_eq!(buf.i, 0);

        assert_eq!(buf.next(), Some("hello"));

        dbg!(&buf);
        buf.cut();
        assert_eq!(to_vec_string(buf), ["world", "foo"]);
    }
}
