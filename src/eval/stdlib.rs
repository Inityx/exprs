use std::rc::Rc;
use crate::{parse::Parser, repr::{Expr, types::{Function, Stream}}};

use super::{env::Env, eval, builtin::BuiltIn, StreamEvalExt};

const DEFINITIONS: &[(&str, &str)] = &[
    ("pi", "3.141592653589793"),
    ("e", "2.718281828459045"),

    ("factorial", "(lambda (n)
        (cond
            ((!= 0 (frac n)) 'undefined)
            ((< n 1) 'undefined)
            ((= n 1) 1)
            ('t (* n (factorial (- n 1))))
        )
    )"),

    ("identity", "(lambda (x) x)"),
    ("and", "(lambda (x y) (cond (x (cond (y 't)))))"),
    ("or", "(lambda (x y) (cond (x 't) (y 't)))"),
    ("not", "(lambda (x) (cond (x ()) ('t 't)))"),

    ("reverse", "(lambda (items)
        (cond
            ((eq items ()) ())
            ('t (concatenate 'list
                (reverse (cdr items))
                (cons (car items) ())
            ))
        )
    )"),
    ("reduce", "(lambda (func acc items)
        (cond
            ((eq items ()) acc)
            ('t (reduce
                func
                (func acc (car items))
                (cdr items)
            ))
        )
    )"),
    ("map", "(lambda (func items)
        (cond
            ((eq items ()) ())
            ('t (cons
                (func (car items))
                (map func (cdr items))
            ))
        )
    )"),
    ("filter", "(lambda (pred items)
        (cond
            ((eq items ()) ())
            ((pred (car items)) (cons
                (car items)
                (filter pred (cdr items))
            ))
            ('t (filter pred (cdr items)))
        )
    )"),
    ("any", "(lambda (pred items)
        (not (eq () (filter pred items)))
    )"),
    ("length", "(lambda (items)
        (eval (cons
            +
            (map (lambda (_) 1) items)
        ))
    )"),
];

const ALIASES: &[(&str, &str)] = &[
    ("setq", "define"),
];

pub fn stdlib() -> Rc<Env> {
    let env = Env::empty();

    env.define("stdin", Expr::from(Stream::stdin()));
    env.define("stdout", Expr::from(Stream::stdout()));

    for name in BuiltIn::all() {
        env.define(
            name,
            Expr::from(Function::from(
                name.parse::<BuiltIn>().expect(name)
            ))
        );
    }

    for (symbol, expr) in DEFINITIONS {
        match Parser::parse(expr) {
            Err(err) => panic!("Failed parsing {}: {}", symbol, err),
            Ok(exprs) => match exprs.last() {
                None => panic!("Failed parsing incomplete expr {}", symbol),
                Some(expr) => match eval(&env, expr) {
                    Err(err) => panic!("Failed evaluating expr {}: {}", symbol, err),
                    Ok(expr) => env.define(symbol, expr),
                }
            }
        }
    }

    for (new, original) in ALIASES {
        let value = env.lookup(original).unwrap();
        env.define(new, value);
    }

    env
}

#[cfg(test)]
mod test {
    use super::*;

    fn eval(exprs: &str) -> String {
        Parser::parse(exprs).unwrap().into_iter()
            .map(|expr| crate::eval::eval(&stdlib(), &expr).unwrap().to_string())
            .last().unwrap()
    }

    #[test]
    fn factorial() {
        assert_eq!(eval("(factorial 0)"), "undefined");
        assert_eq!(eval("(factorial 1.5)"), "undefined");
        assert_eq!(eval("(factorial 1)"), "1");
        assert_eq!(eval("(factorial 4)"), "24");
        assert_eq!(eval("(factorial 10)"), "3628800");
    }

    #[test]
    fn identity() {
        assert_eq!(eval("(identity 5)"), "5");
        assert_eq!(eval("(identity 'foo)"), "foo");
        assert_eq!(eval("(identity (+ 2 2))"), "4");
        assert_eq!(eval("(identity '(1 2 3))"), "(1 2 3)");
    }

    #[test]
    fn boolean() {
        assert_eq!(eval("(and nil nil)"), "()");
        assert_eq!(eval("(and 't nil)"), "()");
        assert_eq!(eval("(and nil 't)"), "()");
        assert_eq!(eval("(and 't 't)"), "t");

        assert_eq!(eval("(or nil nil)"), "()");
        assert_eq!(eval("(or 't nil)"), "t");
        assert_eq!(eval("(or nil 't)"), "t");
        assert_eq!(eval("(or 't 't)"), "t");

        assert_eq!(eval("(not nil)"), "t");
        assert_eq!(eval("(not 't)"), "()");
    }

    #[test]
    fn reverse() {
        assert_eq!(eval("(reverse ())"), "()");
        assert_eq!(eval("(reverse '(1))"), "(1)");
        assert_eq!(eval("(reverse '(1 2 3))"), "(3 2 1)");
    }

    #[test]
    fn reduce() {
        assert_eq!(eval("(reduce (lambda (i j) (+ i j)) 0 ())"), "0");
        assert_eq!(eval("(reduce (lambda (i j) (+ i j)) 0 '(1))"), "1");
        assert_eq!(eval("(reduce (lambda (i j) (+ i j)) 0 '(1 2 3))"), "6");
    }

    #[test]
    fn map() {
        assert_eq!(eval("(map (lambda (i) (+ i 1)) ())"), "()");
        assert_eq!(eval("(map (lambda (i) (+ i 1)) '(1 2 3))"), "(2 3 4)");
    }

    #[test]
    fn filter() {
        assert_eq!(eval("(filter identity ())"), "()");
        assert_eq!(eval("(filter identity '(nil 1 2 nil 3))"), "(1 2 3)");
    }

    #[test]
    fn any() {
        assert_eq!(eval("(any identity ())"), "()");
        assert_eq!(eval("(any (lambda (x) (eq x 2)) '(1 3 4))"), "()");
        assert_eq!(eval("(any (lambda (x) (eq x 2)) '(1 2 4))"), "t");
    }

    #[test]
    fn length() {
        assert_eq!(eval("(length ())"), "0");
        assert_eq!(eval("(length '(1))"), "1");
        assert_eq!(eval("(length '(1 2 3 4 5))"), "5");
    }
}
