use std::{fmt::{Display, self}, rc::Rc, str::FromStr};
use crate::{repr::{Expr, types::{Cons, Function, Quoted, Stryng}, FunctionName}, eval::{Env, ListItems, error::ArgCountErrorExt}};
use super::{eval, EvalResult, ExprEvalExt, ArgList};

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Core {
    Atom, Car, Cdr, Concat, Cond, Cons, Define, Eq, Eval, Lambda, Let, Quote,
}

impl Core {
    pub fn all() -> impl Iterator<Item=&'static str> {
        "atom car cdr concatenate cond cons define eq eval lambda let quote".split_ascii_whitespace()
    }
}

impl Core {
    pub(super) fn eval(&self, env: &Rc<Env>, mut args: ListItems) -> EvalResult<Expr> {
        match self {
            Self::Atom => Ok(Expr::from(
                match args.evaled_to_vec(env)?.exactly_one(self)? {
                    Expr::Nil | Expr::Stryng(_) | Expr::Number(_) | Expr::Symbol(_) => true,
                    Expr::Quoted(_) | Expr::Cons(_) | Expr::Function(_) | Expr::Stream(_) => false,
                }
            )),
            Self::Car => {
                let arg = args.evaled_to_vec(env)?.exactly_one(self)?;
                let Some(cons) = arg.as_cons() else {
                    return Err(arg.type_mismatch(self, "a list"));
                };
                Ok(Expr::from(cons.map(Cons::head_expr).cloned()))
            }
            Self::Cdr => {
                let arg = args.evaled_to_vec(env)?.exactly_one(self)?;
                let Some(cons) = arg.as_cons() else {
                    return Err(arg.type_mismatch(self, "a list"));
                };

                Ok(Expr::from(cons.map(Cons::tail_expr).cloned()))
            }
            Self::Concat => {
                let mut args = args.evaled_to_vec(env)?;
                let Some(arg0) = args.pop_front() else {
                    return Err(self.too_many_args());
                };
                let Some(typename) = arg0.as_symbol() else {
                    return Err(arg0.type_mismatch(self, "a symbol declaring the type"));
                };
                match typename.name() {
                    "string" => args.into_iter()
                        .try_fold(String::new(), |string, expr| match expr.as_string() {
                            Some(suffix) => Ok(string + suffix.as_str()),
                            None => Err(expr.type_mismatch(self, "a string")),
                        })
                        .map(Stryng::from)
                        .map(Expr::from),
                    "list" => args.into_iter()
                        .try_fold(Vec::new(), |mut list, list_expr| {
                            let Some(list_items) = list_expr.parse_list_items() else {
                                return Err(list_expr.type_mismatch(self, "a list"));
                            };
                            list.extend(list_items.cloned_to_vec()?);
                            Ok(list)
                        })
                        .map(Expr::new_list),
                    _ => Err(Expr::from(typename.clone()).type_mismatch(self, "'list or 'string")),
                }
            }
            Self::Cond => {
                for branch in args {
                    let branch = branch?;
                    let Some(mut branch_args) = branch.parse_list_items() else {
                        return Err(branch.clone().type_mismatch(self, "a list"));
                    };

                    if let Some(cond_expr) = branch_args.next() {
                        let cond_value = eval(env, cond_expr?)?;

                        if cond_value != Expr::Nil {
                            let mut evaled_statements = branch_args.evaled_to_vec(env)?;
                            return Ok(evaled_statements.pop().unwrap_or(cond_value));
                        }
                    }
                }

                Ok(Expr::Nil)
            }
            Self::Cons => Ok(Expr::from(
                args.evaled_to_vec(env)?.exactly::<2>(self)?
            )),
            Self::Eq => {
                let [left, right] = args.evaled_to_vec(env)?.exactly(self)?;
                Ok(Expr::from(left == right))
            },
            Self::Eval => {
                let form = args.evaled_to_vec(env)?.exactly_one(self)?;
                eval(env, &form)
            }
            Self::Define => {
                let [name, value_expr] = args.cloned_to_vec()?.exactly(self)?;
                let Some(symbol) = name.as_symbol() else {
                    return Err(name.type_mismatch(self, "a symbol"));
                };
                let value = eval(env, &value_expr)?;

                env.define(symbol.name(), value);
                Ok(symbol.clone().into())
            }
            Self::Lambda => {
                let mut args = args.cloned_to_vec()?;
                let Some(first_arg) = args.pop_front() else {
                    return Err(self.not_enough_args());
                };

                let mut proc_args = Vec::new();
                let Some(param_exprs) = first_arg.parse_list_items() else {
                    return Err(first_arg.type_mismatch(self, "a list"));
                };
                for param_expr in param_exprs {
                    let param_expr = param_expr?;
                    let Some(proc_arg_symbol) = param_expr.as_symbol() else {
                        return Err(param_expr.clone().type_mismatch(self, "a symbol"));
                    };
                    proc_args.push(proc_arg_symbol);
                }

                Ok(Expr::from(Function::proc(
                    Rc::clone(env),
                    &proc_args,
                    args
                )))
            }
            Self::Let => {
                let local_env = env.child();

                let Some(defs_list_expr) = args.next() else {
                    return Err(self.not_enough_args());
                };
                let defs_list_expr = defs_list_expr?;
                let Some(defs_exprs) = defs_list_expr.parse_list_items() else {
                    return Err(defs_list_expr.clone().type_mismatch(&"let definitions list", "a list of definitions"));
                };

                for def_expr in defs_exprs {
                    let def_expr = def_expr?;
                    let Some(def) = def_expr.parse_list_items() else {
                        return Err(def_expr.clone().type_mismatch(&"let definition", "a definition pair"));
                    };
                    let [var_expr, value_expr] = def.cloned_to_vec()?.exactly(&"let definition")?;

                    let Some(var_symbol) = var_expr.as_symbol() else {
                        return Err(var_expr.type_mismatch(self, "symbol"));
                    };

                    let evaled_value = eval(&local_env, &value_expr)?;
                    local_env.define(var_symbol.name(), evaled_value);
                }

                Ok(args.evaled_to_vec(&local_env)?.pop().unwrap_or(Expr::Nil))
            }
            Self::Quote => Ok(Expr::from(Quoted::new(
                args.cloned_to_vec()?.exactly_one(self)?
            ))),
        }
    }
}

impl FunctionName for Core {
    fn function_name(&self) -> &'static str {
        match self {
            Self::Atom => "atom",
            Self::Car => "car",
            Self::Cdr => "cdr",
            Self::Concat => "concatenate",
            Self::Cond => "cond",
            Self::Cons => "cons",
            Self::Define => "define",
            Self::Eq => "eq",
            Self::Eval => "eval",
            Self::Lambda => "lambda",
            Self::Let => "let",
            Self::Quote => "quote",
        }
    }
}

impl FromStr for Core {
    type Err = ();

    fn from_str(name: &str) -> ::std::result::Result<Self, Self::Err> {
        Ok(match name {
            "atom" => Self::Atom,
            "car" => Self::Car,
            "cdr" => Self::Cdr,
            "cond" => Self::Cond,
            "cons" => Self::Cons,
            "concatenate" => Self::Concat,
            "define" => Self::Define,
            "eq" => Self::Eq,
            "eval" => Self::Eval,
            "lambda" => Self::Lambda,
            "let" => Self::Let,
            "quote" => Self::Quote,
            _ => return Err(()),
        })
    }
}

impl Display for Core {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.function_name())
    }
}

#[cfg(test)]
mod test {
    use std::assert_matches::assert_matches;
    use crate::{repr::types::Symbol, eval::Error};
    use super::*;

    fn eval_default(src: &str) -> EvalResult<Expr> {
        let expr = crate::parse::Parser::parse(src).unwrap()[0].clone();
        eval(&Env::std(), &expr)
    }

    #[test]
    fn atom() {
        assert_eq!(
            eval_default("(atom 2)"),
            Ok(Expr::from(Symbol::TRUE)),
        );
    }

    #[test]
    fn concatenate() {
        assert_eq!(
            eval_default("(concatenate 'list () ())"),
            Ok(Expr::Nil),
        );
        assert_eq!(
            eval_default("(concatenate 'list () '(foo))"),
            Ok(Expr::new_list([Expr::from(Symbol::from("foo"))])),
        );
        assert_eq!(
            eval_default("(concatenate 'list '(foo) ())"),
            Ok(Expr::new_list([Expr::from(Symbol::from("foo"))])),
        );
        assert_eq!(
            eval_default("(concatenate 'list '(1 2 3) '(4 5 6))"),
            Ok(Expr::new_list((1..7u8).map(f64::from).map(Expr::from))),
        );
        assert_eq!(
            eval_default(r#"(concatenate 'string "hello" " " "world")"#),
            Ok(Expr::from(Stryng::from("hello world"))),
        );
        assert_matches!(
            eval_default(r#"(concatenate 'list "hello" " " "world")"#),
            Err(Error::TypeMismatch { .. }),
        );
    }

    #[test]
    fn cond() {
        assert_eq!(
            eval_default("(cond)"),
            Ok(Expr::Nil),
        );

        assert_eq!(
            eval_default("(cond (nil 1) ('foo 2.0) (nil 3))"),
            Ok(Expr::from(2.0)),
        );

        assert_eq!(
            eval_default("(cond ('foo))"),
            Ok(Expr::from(Symbol::from("foo"))),
        );
    }

    #[test]
    fn r#let() {
        assert_eq!(
            eval_default("(let ((two 2) (four (+ two two))) (+ four two))"),
            Ok(Expr::from(6.0)),
        );
    }
}
