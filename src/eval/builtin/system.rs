use std::{str::FromStr, rc::Rc, fmt, io::{BufReader, BufRead}, slice};
use crate::{
    eval::{Env, EvalResult, ExprEvalExt, ArgList, Error, ListItems, StreamEvalExt, error::ArgCountErrorExt},
    repr::{Expr, types::{Stream, Stryng}, FunctionName},
};

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum System {
    Open,
    NewInputBuffer,
    NewOutputBuffer,
    UnwrapOutputBuffer,
    Read(Read),
    Write(Write),
}

impl fmt::Display for System {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.function_name())
    }
}

impl FromStr for System {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "open" => Self::Open,
            "make-string-input-stream" => Self::NewInputBuffer,
            "make-string-output-stream" => Self::NewOutputBuffer,
            "get-output-stream-string" => Self::UnwrapOutputBuffer,
            "read"       => Self::Read (Read::Obj),
            "read-char"  => Self::Read (Read::Char),
            "read-line"  => Self::Read (Read::Line),
            "write"      => Self::Write(Write::Obj),
            "write-char" => Self::Write(Write::Char),
            "write-line" => Self::Write(Write::Line),
            _ => return Err(()),
        })
    }
}

impl System {
    pub(super) fn all() -> impl Iterator<Item=&'static str> {
        "
            open
            make-string-input-stream make-string-output-stream get-output-stream-string
            read read-char read-line
            write write-char write-line
        ".split_ascii_whitespace()
    }

    pub(super) fn eval(&self, env: &Rc<Env>, args: ListItems) -> EvalResult<Expr> {
        let mut evaled_args = args.evaled_to_vec(env)?;

        match self {
            Self::Open => {
                let arg0 = evaled_args.exactly_one(self)?;
                let Some(file_name) = arg0.as_string() else {
                    return Err(arg0.type_mismatch(self, "a string"));
                };
                Ok(Expr::from(Stream::open(file_name.as_str())))
            }
            Self::NewInputBuffer => {
                let arg0 = evaled_args.exactly_one(self)?;
                let Some(contents) = arg0.as_string() else {
                    return Err(arg0.type_mismatch(self, "a string"));
                };
                Ok(Expr::from(Stream::buffer(contents.as_str())))
            }
            Self::NewOutputBuffer => Ok(Expr::from(Stream::buffer(""))),
            Self::UnwrapOutputBuffer => {
                let buffer = evaled_args
                    .exactly_one(self)?
                    .into_stream(self)?
                    .get_buffer(self)?;
                Ok(Expr::from(Stryng::from(
                    std::str::from_utf8(&buffer).unwrap()
                )))
            }
            Self::Read(read) => {
                if evaled_args.len() > 1 { return Err(self.too_many_args()); }

                let stream;
                let mut read_lock;
                let mut stdin;
                let stream = match evaled_args.pop() {
                    Some(expr) => {
                        stream = expr.into_stream(self)?;
                        read_lock = stream.as_read(self)?;
                        &mut *read_lock
                    }
                    None => {
                        stdin = std::io::stdin();
                        &mut stdin
                    }
                };

                read.from(stream)
            }
            Self::Write(write) => {
                let Some(obj_arg) = evaled_args.pop_front() else { return Err(self.not_enough_args()); };

                if evaled_args.len() > 1 { return Err(self.too_many_args()); }

                let stream;
                let mut write_lock;
                let mut stdout;
                let stream = match evaled_args.pop() {
                    Some(stream_arg) => {
                        stream = stream_arg.into_stream(self)?;
                        write_lock = stream.as_write(self)?;
                        &mut *write_lock
                    }
                    None => {
                        stdout = std::io::stdout();
                        &mut stdout
                    }
                };

                write.to(stream, obj_arg)
            }
        }
    }
}

impl FunctionName for System {
    fn function_name(&self) -> &'static str {
        match self {
            Self::Open => "open",
            Self::NewInputBuffer => "make-string-input-stream",
            Self::NewOutputBuffer => "make-string-output-stream",
            Self::UnwrapOutputBuffer => "get-output-stream-string",
            Self::Read(read) => read.function_name(),
            Self::Write(write) => write.function_name(),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Read { Obj, Char, Line }

impl Read {
    fn from(&self, stream: &mut dyn std::io::Read) -> EvalResult<Expr> {
        match self {
            Read::Obj => todo!(),
            Read::Char => {
                let mut buf = [0u8;4];
                let mut i = 0;

                loop {
                    if stream.read(slice::from_mut(&mut buf[i])).unwrap() == 0 {
                        return Err(Error::StreamEnded);
                    }

                    if let Ok(s) = std::str::from_utf8(&buf[0..i+1]) {
                        break Ok(Expr::from(Stryng::from(s.to_string())));
                    }

                    if i >= buf.len() {
                        break Err(Error::StreamEnded);
                    }

                    i += 1;
                }
            },
            Read::Line => {
                let mut buf = String::new();

                BufReader::new(stream).read_line(&mut buf).unwrap(); // pretty sure this is incorrect
                if buf.ends_with('\n') { buf.pop(); }

                Ok(Expr::from(Stryng::from(buf)))
            }
        }
    }
}

impl FunctionName for Read {
    fn function_name(&self) -> &'static str {
        match self {
            Self::Obj => "read",
            Self::Char => "read-char",
            Self::Line => "read-line",
        }
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Write { Obj, Char, Line }

impl Write {

    fn to(&self, stream: &mut dyn std::io::Write, obj_arg: Expr) -> EvalResult<Expr> {
        match self {
            Write::Obj => {
                write!(stream, "{}", obj_arg).unwrap();
                Ok(obj_arg)
            }
            Write::Char => {
                let Some(string) = obj_arg.as_string() else {
                    return Err(obj_arg.type_mismatch(self, "a single-character string"));
                };
                let Some(ch) = string.as_str().chars().next() else {
                    return Err(Error::InvalidArgumentBecause("Empty string in write-char".to_string()));
                };
                let ch = ch.to_string();
                stream.write_all(ch.as_bytes()).unwrap();
                Ok(Expr::from(Stryng::from(ch)))
            }
            Write::Line => {
                let Some(string) = obj_arg.as_string() else {
                    return Err(obj_arg.type_mismatch(self, "a string"));
                };
                stream.write_all(string.as_str().as_bytes()).unwrap();
                stream.write_all(b"\n").unwrap();
                Ok(Expr::from(string.clone()))
            }
        }
    }
}

impl FunctionName for Write {
    fn function_name(&self) -> &'static str {
        match self {
            Self::Obj => "write",
            Self::Char => "write-char",
            Self::Line => "write-line",
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{eval::eval, repr::types::Stryng};
    use super::*;

    fn eval_default(src: &str) -> EvalResult<Expr> {
        let expr = crate::parse::Parser::parse(src).unwrap()[0].clone();
        eval(&Env::std(), &expr)
    }

    #[test]
    fn string_output() {
        assert_eq!(
            eval_default(r#"(let
                ((s (make-string-output-stream)))
                (write-line "hello" s)
                (write-line "world" s)
                (get-output-stream-string s)
            )"#),
            Ok(Expr::from(Stryng::from("hello\nworld\n"))),
        );
    }

    #[test]
    fn string_intput() {
        assert_eq!(
            eval_default("(let
                ((s (make-string-input-stream \"hello\nworld\n\")))
                (write-line \"hello\" s)
                (write-line \"world\" s)
                (get-output-stream-string s)
            )"),
            Ok(Expr::from(Stryng::from("hello\nworld\n"))),
        );
    }
}
