use std::{fmt::{Display, self}, str::FromStr, rc::Rc, convert::identity};
use crate::{
    repr::{types::Number, Expr, FunctionName},
    eval::{Env, ExprEvalExt, ArgList, ListItems, error::ArgCountErrorExt},
};
use super::EvalResult;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Numeric {
    Unary(Unary),
    Reduce(Reduce),
    Compare(Compare),
}

impl Numeric {
    pub(super) fn all() -> impl Iterator<Item=&'static str> {
        "inc dec int frac + - * / min max < <= = != >= >".split_ascii_whitespace()
    }
}

impl Numeric {
    pub(super) fn eval(&self, env: &Rc<Env>, args: ListItems) -> EvalResult<Expr> {
        let numbers = args.evaled_to_vec(env)?.into_iter()
            .map(|arg| match arg {
                Expr::Number(value) => Ok(value),
                expr => Err(expr.type_mismatch(self, "numbers")),
            })
            .collect::<EvalResult<Vec<Number>>>()?;

        Ok(match self {
            Self::Unary(unary) => {
                let arg = numbers.exactly_one(self)?;
                Expr::from(unary.func()(arg))
            }
            Self::Reduce(reduce) => Expr::from(reduce.func()(&numbers)?),
            Self::Compare(compare) => {
                let comparator = compare.func();
                Expr::from(
                    numbers.array_chunks()
                        .map(|[l, r]| comparator(l, r))
                        .all(identity)
                )
            }
        })
    }
}

impl FunctionName for Numeric {
    fn function_name(&self) -> &'static str {
        match self {
            Self::Unary(inner) => match inner {
                Unary::Inc => "inc",
                Unary::Dec => "dec",
                Unary::Int => "int",
                Unary::Frac => "frac",
            }
            Self::Reduce(inner) => match inner {
                Reduce::Add => "+",
                Reduce::Sub => "-",
                Reduce::Mul => "*",
                Reduce::Div => "/",
                Reduce::Min => "min",
                Reduce::Max => "max",
            }
            Self::Compare(inner) => match inner {
                Compare::LT => "<",
                Compare::LE => "<=",
                Compare::EQ => "=",
                Compare::NE => "!=",
                Compare::GE => ">=",
                Compare::GT => ">",
            }
        }
    }
}

impl Display for Numeric {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.function_name())
    }
}

impl FromStr for Numeric {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "inc"  => Self::Unary(Unary::Inc),
            "dec"  => Self::Unary(Unary::Dec),
            "int"  => Self::Unary(Unary::Int),
            "frac" => Self::Unary(Unary::Frac),
            "+"    => Self::Reduce(Reduce::Add),
            "-"    => Self::Reduce(Reduce::Sub),
            "*"    => Self::Reduce(Reduce::Mul),
            "/"    => Self::Reduce(Reduce::Div),
            "min"  => Self::Reduce(Reduce::Min),
            "max"  => Self::Reduce(Reduce::Max),
            "<"    => Self::Compare(Compare::LT),
            "<="   => Self::Compare(Compare::LE),
            "="    => Self::Compare(Compare::EQ),
            "!="   => Self::Compare(Compare::NE),
            ">="   => Self::Compare(Compare::GE),
            ">"    => Self::Compare(Compare::GT),
            _ => return Err(()),
        })
    }
}


#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Unary { Inc, Dec, Int, Frac }

impl Unary {
    fn func(&self) -> fn(Number) -> Number {
        match self {
            Unary::Inc => |x| x + 1.0,
            Unary::Dec => |x| x - 1.0,
            Unary::Int => Number::trunc,
            Unary::Frac => Number::fract,
        }
    }
}


#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Reduce { Add, Sub, Mul, Div, Min, Max }

impl Reduce {
    fn func(&self) -> fn(&[Number]) -> EvalResult<Number> {
        use std::ops::{Div, Sub};

        match self {
            Self::Add => |slice| Ok(slice.iter().sum()),
            Self::Mul => |slice| Ok(slice.iter().product()),
            Self::Sub => |slice| match slice {
                [] => Err("-".not_enough_args()),
                [num] => Ok(-num),
                nums => Ok(nums.iter().copied().reduce(Number::sub).unwrap()),
            },
            Self::Div => |slice| match slice {
                [] => Err("/".not_enough_args()),
                [num] => Ok(num.recip()),
                nums => Ok(nums.iter().copied().reduce(Number::div).unwrap()),
            },
            Self::Min => |slice| match slice {
                [] => Err("min".not_enough_args()),
                nums => Ok(nums.iter().copied().reduce(|l, r| if l < r { l } else { r }).unwrap()),
            },
            Self::Max => |slice| match slice {
                [] => Err("max".not_enough_args()),
                nums => Ok(nums.iter().copied().reduce(|l, r| if l > r { l } else { r }).unwrap()),
            }
        }
    }
}



#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Compare { LT, LE, EQ, NE, GE, GT }

impl Compare {
    fn func(&self) -> fn(&Number, &Number) -> bool {
        match self {
            Compare::LT => Number::lt,
            Compare::LE => Number::le,
            Compare::EQ => Number::eq,
            Compare::NE => Number::ne,
            Compare::GE => Number::ge,
            Compare::GT => Number::gt,
        }
    }
}

#[cfg(test)]
mod test {
    use crate::eval::eval;
    use super::*;

    fn eval_default(src: &str) -> EvalResult<Expr> {
        let expr = crate::parse::Parser::parse(src).unwrap()[0].clone();
        eval(&Env::std(), &expr)
    }

    #[test]
    fn add() {
        assert_eq!(
            eval_default("(+)"),
            Ok(Expr::from(0.0)),
        );
        assert_eq!(
            eval_default("(+ 4 1 2)"),
            Ok(Expr::from(7.0)),
        );
    }

    #[test]
    fn sub() {
        assert_eq!(
            eval_default("(-)"),
            Err("-".not_enough_args()),
        );
        assert_eq!(
            eval_default("(- 4)"),
            Ok(Expr::from(-4.0)),
        );
        assert_eq!(
            eval_default("(- 4 1 2)"),
            Ok(Expr::from(1.0)),
        );
    }

    #[test]
    fn mul() {
        assert_eq!(
            eval_default("(*)"),
            Ok(Expr::from(1.0)),
        );
        assert_eq!(
            eval_default("(* 4 1 2)"),
            Ok(Expr::from(8.0)),
        );
    }

    #[test]
    fn div() {
        assert_eq!(
            eval_default("(/)"),
            Err("/".not_enough_args()),
        );
        assert_eq!(
            eval_default("(/ 4)"),
            Ok(Expr::from(0.25)),
        );
        assert_eq!(
            eval_default("(/ 4 1 2)"),
            Ok(Expr::from(2.0)),
        );
    }

    #[test]
    fn nested_arithmetic() {
        assert_eq!(
            eval_default("(* 2 (+ 3 4))"),
            Ok(Expr::from(14.0)),
        );
    }

}
