mod numeric;
mod system;
mod core;

use std::{rc::Rc, fmt, str::FromStr};
use crate::repr::Expr;

use self::{system::System, numeric::Numeric, core::Core};

use super::{
    eval,
    EvalResult,
    env::Env, ExprEvalExt, ArgList, ListItems,
};

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BuiltIn {
    Core(Core),
    Numeric(Numeric),
    System(System)
}

impl BuiltIn {
    pub fn all() -> impl Iterator<Item=&'static str> {
        Core::all().chain(Numeric::all()).chain(System::all())
    }

    pub(super) fn eval(&self, env: &Rc<Env>, args: ListItems) -> EvalResult<Expr> {
        match self {
            Self::Core(c) => c.eval(env, args),
            Self::Numeric(n) => n.eval(env, args),
            Self::System(s) => s.eval(env, args),
        }
    }
}

impl FromStr for BuiltIn {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Core::from_str(s).map(Self::Core)
            .or_else(|()| Numeric::from_str(s).map(Self::Numeric))
            .or_else(|()| System::from_str(s).map(Self::System))
    }
}

impl fmt::Display for BuiltIn {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Core(c) => c.fmt(f),
            Self::Numeric(n) => n.fmt(f),
            Self::System(s) => s.fmt(f),
        }
    }
}

#[cfg(test)]
mod test {
    use std::assert_matches::assert_matches;
    use super::*;

    #[test]
    fn parse() {
        assert_eq!(BuiltIn::from_str("atom"), Ok(BuiltIn::Core(Core::Atom)));
        assert_matches!(BuiltIn::from_str("!="), Ok(BuiltIn::Numeric(Numeric::Compare(_))));
        assert_matches!(BuiltIn::from_str("write"), Ok(BuiltIn::System(System::Write(_))));
    }
}
