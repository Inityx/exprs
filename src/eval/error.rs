use std::{fmt::Display, borrow::Cow};

use crate::repr::{Expr, FunctionName};

#[derive(Debug, PartialEq)]
pub enum Error {
    TypeMismatch {
        context: &'static str,
        expected: &'static str,
        got: Expr
    },
    SymbolNotFound(String),
    NotEnoughArgsFor(Cow<'static, str>),
    TooManyArgsFor(Cow<'static, str>),
    NotAFunction(Expr),
    ImproperListEndsWith(Expr),
    InvalidArgumentBecause(String),
    StreamEnded,
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::TypeMismatch { context, expected, got } => write!(
                f,
                "Type mismatch in {}: expected {}, got {} `{}`",
                context, expected, got.function_name(), got,
            ),
            Error::SymbolNotFound(symbol) => write!(f, "Undefined symbol {:?}", symbol),
            Error::NotEnoughArgsFor(func) => write!(f, "Not enough args for {}", func),
            Error::TooManyArgsFor(func) => write!(f, "Too many args for {}", func),
            Error::NotAFunction(expr) => write!(f, "Expected function, found {} `{}`", expr.function_name(), expr),
            Error::ImproperListEndsWith(expr) => write!(f, "Improper list encountered (ended with {} `{}`)", expr.function_name(), expr),
            Error::InvalidArgumentBecause(msg) => write!(f, "Invalid argument: {}", msg),
            Error::StreamEnded => write!(f, "Stream ended"),
        }
    }
}

pub trait ArgCountErrorExt {
    fn not_enough_args(&self) -> Error;
    fn too_many_args(&self) -> Error;
}

impl<Named: FunctionName> ArgCountErrorExt for Named {
    fn not_enough_args(&self) -> Error {
        Error::NotEnoughArgsFor(Cow::Borrowed(self.function_name()))
    }

    fn too_many_args(&self) -> Error {
        Error::TooManyArgsFor(Cow::Borrowed(self.function_name()))
    }
}
