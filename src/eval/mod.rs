mod builtin;
mod stdlib;
mod env;
mod error;

use self::error::ArgCountErrorExt;
pub use self::{builtin::BuiltIn, stdlib::stdlib, env::Env, error::Error};
use std::{
    rc::Rc,
    cmp::Ordering,
    fmt,
    convert::TryInto,
    io::{Read, Write, stdin, stdout, Cursor},
    cell::{RefMut, RefCell},
    path::Path,
    fs::File,
};
use crate::repr::{Expr, types::{Cons, Function, Symbol, Stream, Stryng}, FunctionName};

pub type EvalResult<T> = Result<T, Error>;

pub fn eval(env: &Rc<Env>, expr: &Expr) -> EvalResult<Expr> {
    match expr {
        Expr::Quoted(quoted) => Ok(quoted.value().clone()),
        Expr::Symbol(symbol) => env.lookup(symbol.name()),
        Expr::Cons(cons) => {
            let (func, args_expr) = cons.exprs_cloned();
            let args_list = ListItems(&args_expr);

            let (label, function) = match func {
                Expr::Function(function) => ("function", function),
                Expr::Cons(_) => ("function", eval(env, &func)?.into_function()?),
                Expr::Symbol(ref symbol) => (symbol.name(), env.lookup(symbol.name())?.into_function()?),
                _ => return Err(Error::NotAFunction(func)),
            };

            function.call(label, env, args_list)
        }
        _ => Ok(expr.clone()),
    }
}

trait ArgList<T>: Sized + IntoIterator<Item=T> {
    fn exactly<const COUNT: usize>(self, err_context: &impl FunctionName) -> EvalResult<[T;COUNT]>;

    fn exactly_one(self, err_context: &impl FunctionName) -> EvalResult<T> {
        self.exactly::<1>(err_context).map(|[expr]| expr)
    }

    fn pop_front(&mut self) -> Option<T>;
}

impl<T: fmt::Debug> ArgList<T> for Vec<T> {
    fn exactly<const COUNT: usize>(self, err_context: &impl FunctionName) -> EvalResult<[T;COUNT]> {
        match self.len().cmp(&COUNT) {
            Ordering::Equal => Ok(self.try_into().unwrap()),
            Ordering::Less => Err(err_context.not_enough_args()),
            Ordering::Greater => Err(err_context.too_many_args()),
        }
    }

    fn pop_front(&mut self) -> Option<T> {
        (!self.is_empty()).then(|| self.remove(0))
    }
}

trait ExprEvalExt {
    fn as_cons(&self) -> Option<Option<&Cons>>;
    fn as_symbol(&self) -> Option<&Symbol>;
    fn as_string(&self) -> Option<&Stryng>;
    fn into_stream(self, err_context: &impl FunctionName) -> EvalResult<Stream>;
    fn into_function(self) -> EvalResult<Function>;
    fn parse_list_items(&self) -> Option<ListItems<'_>>;
    fn type_mismatch(self, err_context: &impl FunctionName, expected: &'static str) -> Error;
}

impl ExprEvalExt for Expr {
    fn as_cons(&self) -> Option<Option<&Cons>> {
        match self {
            Expr::Cons(cons) => Some(Some(cons)),
            Expr::Nil => Some(None),
            _ => None,
        }
    }

    fn as_symbol(&self) -> Option<&Symbol> {
        match self {
            Self::Symbol(symbol) => Some(symbol),
            _ => None,
        }
    }

    fn as_string(&self) -> Option<&Stryng> {
        match self {
            Self::Stryng(string) => Some(string),
            _ => None,
        }
    }

    fn into_stream(self, err_context: &impl FunctionName) -> EvalResult<Stream> {
        match self {
            Self::Stream(stream) => Ok(stream),
            _ => Err(self.type_mismatch(err_context, "a stream")),
        }
    }

    fn into_function(self) -> EvalResult<Function> {
        match self {
            Self::Function(proc) => Ok(proc),
            _ => Err(Error::NotAFunction(self)),
        }
    }

    fn parse_list_items(&self) -> Option<ListItems<'_>> {
        match self {
            Expr::Nil | Expr::Cons(_) => Some(ListItems(self)),
            _ => None,
        }
    }

    fn type_mismatch(self, err_context: &impl FunctionName, expected: &'static str) -> Error {
        Error::TypeMismatch {
            context: err_context.function_name(),
            expected,
            got: self,
        }
    }
}

struct ListItems<'a>(&'a Expr);

impl<'a> Iterator for ListItems<'a> {
    type Item = EvalResult<&'a Expr>;

    fn next(&mut self) -> Option<Self::Item> {
        let Some(next) = self.0.as_cons() else {
            return Some(Err(Error::ImproperListEndsWith(self.0.clone())));
        };
        let (head, tail) = next?.exprs();
        self.0 = tail;
        Some(Ok(head))
    }
}

impl ListItems<'_> {
    pub fn evaled_to_vec(self, env: &Rc<Env>) -> EvalResult<Vec<Expr>> {
        self.map(|result| result.and_then(|arg| eval(env, arg))).collect()
    }

    pub fn cloned_to_vec(self) -> EvalResult<Vec<Expr>> {
        self.map(|result| result.map(Expr::clone)).collect()
    }
}

trait FunctionEvalExt {
    fn call(&self, label: &str, outer_env: &Rc<Env>, args: ListItems) -> EvalResult<Expr>;
}

impl FunctionEvalExt for Function {
    fn call(&self, label: &str, outer_env: &Rc<Env>, args: ListItems) -> EvalResult<Expr> {
        match self {
            Function::BuiltIn(built_in) => built_in.eval(outer_env, args),
            Function::Proc { env, params, body } => {
                let evaled_args = args.evaled_to_vec(outer_env)?;
                let locals = env.child();

                match evaled_args.len().cmp(&params.len()) {
                    Ordering::Less => return Err(Error::NotEnoughArgsFor(label.to_string().into())),
                    Ordering::Greater => return Err(Error::TooManyArgsFor(label.to_string().into())),
                    Ordering::Equal => {}
                }

                for (param, argument) in params.iter().zip(evaled_args) {
                    eprintln!("Defining parameter {} as {}", param, argument);
                    locals.define(param.as_ref(), argument);
                }

                Ok(body.iter()
                    .map(|form| eval(&locals, form))
                    .collect::<EvalResult<Vec<_>>>()?
                    .pop()
                    .unwrap_or(Expr::Nil))
            }
        }
    }
}

trait StreamEvalExt {
    fn stdin() -> Self;
    fn stdout() -> Self;
    fn open(path: impl AsRef<Path>) -> Self;
    fn buffer(contents: &str) -> Self;
    fn as_read(&self, err_context: &impl FunctionName) -> EvalResult<RefMut<dyn Read>>;
    fn as_write(&self, err_context: &impl FunctionName) -> EvalResult<RefMut<dyn Write>>;
    fn get_buffer(&self, err_context: &impl FunctionName) -> EvalResult<Vec<u8>>;
}

impl StreamEvalExt for Stream {
    fn stdin() -> Self {
        Self::Stdin(Rc::new(RefCell::new(stdin())))
    }

    fn stdout() -> Self {
        Self::Stdout(Rc::new(RefCell::new(stdout())))
    }

    fn open(path: impl AsRef<Path>) -> Self {
        Self::File {
            name: path.as_ref().display().to_string().into(),
            file: Rc::new(RefCell::new(File::open(path).unwrap())),
        }
    }

    fn buffer(contents: &str) -> Self {
        Self::Buffer(Rc::new(RefCell::new(Cursor::new(
            contents.as_bytes().to_owned()
        ))))
    }

    fn as_read(&self, err_context: &impl FunctionName) -> EvalResult<RefMut<dyn Read>> {
        match self {
            Self::Stdin(stdin) => Ok(stdin.borrow_mut()),
            Self::File { file, .. } => Ok(file.borrow_mut()),
            Self::Buffer(buffer) => Ok(buffer.borrow_mut()),
            Self::Stdout(_) => Err(Expr::from(self.clone()).type_mismatch(err_context, "an input stream")),
        }
    }

    fn as_write(&self, err_context: &impl FunctionName) -> EvalResult<RefMut<dyn Write>> {
        match self {
            Self::Stdout(w) => Ok(w.borrow_mut()),
            Self::File { file, .. } => Ok(file.borrow_mut()),
            Self::Buffer(buffer) => Ok(buffer.borrow_mut()),
            Self::Stdin(_) => Err(Expr::from(self.clone()).type_mismatch(err_context, "an output stream")),
        }
    }

    fn get_buffer(&self, err_context: &impl FunctionName) -> EvalResult<Vec<u8>> {
        match self {
            Self::Buffer(buffer) => Ok(buffer.as_ref().clone().into_inner().into_inner()),
            _ => Err(Expr::from(self.clone()).type_mismatch(err_context, "a string buffer stream")),
        }
    }
}
