use std::{cell::RefCell, rc::Rc, collections::BTreeMap};

use crate::repr::Expr;
use super::{stdlib::stdlib, EvalResult, Error};

#[derive(PartialEq)]
pub struct Env {
    locals: RefCell<BTreeMap<Box<str>, Expr>>,
    parent: Option<Rc<Env>>,
}

impl Env {
    pub fn std() -> Rc<Self> {
        stdlib()
    }

    pub fn empty() -> Rc<Self> {
        Rc::new(Self { locals: Default::default(), parent: None })
    }

    pub fn child(self: &Rc<Self>) -> Rc<Self> {
        Rc::new(Env {
            locals: Default::default(),
            parent: Some(Rc::clone(self)),
        })
    }

    pub fn lookup(&self, symbol: &str) -> EvalResult<Expr> {
        if let Some(local) = self.locals.borrow().get(symbol) {
            Ok(local.clone())
        } else if let Some(parent) = &self.parent {
            parent.lookup(symbol)
        } else {
            Err(Error::SymbolNotFound(symbol.to_string()))
        }
    }

    pub fn define(&self, symbol: &str, value: Expr) {
        self.locals.borrow_mut().insert(Box::from(symbol), value);
    }
}
