use std::{rc::Rc, borrow::Cow, fmt, io::{Stdin, Stdout, Cursor}, cell::{RefCell}, fs::File};
use crate::eval::{Env, BuiltIn};
use super::{Expr, FunctionName};

pub type Number = f64;

#[derive(Clone, Debug, PartialEq)]
pub struct Cons {
    pub head: Rc<Expr>,
    pub tail: Rc<Expr>,
}

impl Cons {
    pub fn new(value: Expr, next: Expr) -> Self { Self { head: Rc::new(value), tail: Rc::new(next) } }
    pub fn head_expr(&self) -> &Expr { self.head.as_ref() }
    pub fn tail_expr(&self) -> &Expr { self.tail.as_ref() }
    pub fn exprs(&self) -> (&Expr, &Expr) { (self.head_expr(), self.tail_expr()) }
    pub fn exprs_cloned(&self) -> (Expr, Expr) { (self.head_expr().clone(), self.tail_expr().clone()) }
}


#[derive(Clone, Debug, PartialEq)]
pub struct Quoted(pub Rc<Expr>);

impl Quoted {
    pub fn new(expr: Expr) -> Self { Self(Rc::new(expr)) }
    pub fn value(&self) -> &Expr { self.0.as_ref() }
}


#[derive(Clone, Debug, PartialEq)]
pub struct Symbol(pub Cow<'static, str>);

impl Symbol {
    pub const TRUE: Self = Self(Cow::Borrowed("t"));
    pub fn name(&self) -> &str { self.0.as_ref() }
}

impl From<&'static str> for Symbol {
    fn from(name: &'static str) -> Self { Self(Cow::Borrowed(name)) }
}

impl From<std::string::String> for Symbol {
    fn from(name: std::string::String) -> Self { Self(Cow::Owned(name)) }
}


#[derive(Clone, Debug, PartialEq)]
pub struct Stryng(Rc<str>);

impl Stryng {
    pub fn as_str(&self) -> &str { self.0.as_ref() }
}

impl<S: AsRef<str>> From<S> for Stryng {
    fn from(value: S) -> Self { Self(Rc::from(value.as_ref())) }
}


#[derive(Clone, PartialEq)]
pub enum Function {
    Proc {
        env: Rc<Env>,
        params: Box<[Box<str>]>,
        body: Vec<Expr>,
    },
    BuiltIn(BuiltIn),
}

impl Function {
    pub fn proc(env: Rc<Env>, args: &[&Symbol], body: Vec<Expr>) -> Self {
        let params = args.iter().copied()
            .map(Symbol::name)
            .map(str::to_string)
            .map(String::into_boxed_str)
            .collect();

        Self::Proc { env, body, params }
    }
}

impl FunctionName for Function {
    fn function_name(&self) -> &'static str {
        match self {
            Self::Proc { .. } => "function object",
            Self::BuiltIn(_) => "built-in function",
        }
    }
}

impl fmt::Debug for Function {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Proc { params: args, body, .. } => write!(f, "Proc({:?}, {:?})", args, body),
            Self::BuiltIn(built_in) => write!(f, "BuiltIn({:?})", built_in),
        }
    }
}

impl fmt::Display for Function {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "#<")?;

        match self {
            Self::BuiltIn(built_in) => write!(f, "built-in {}", built_in)?,
            Self::Proc { params, .. } => {
                write!(f, "function (")?;

                let mut iter = params.iter();
                if let Some(item) = iter.next() { write!(f, "{}", item)?; }
                for item in iter { write!(f, ", {}", item)?; }

                write!(f, ")")?;
            }
        }

        write!(f, ">")
    }
}

impl From<BuiltIn> for Function {
    fn from(inner: BuiltIn) -> Self { Self::BuiltIn(inner) }
}

#[derive(Debug, Clone)]
pub enum Stream {
    Stdin(Rc<RefCell<Stdin>>),
    Stdout(Rc<RefCell<Stdout>>),
    File { name: Box<str>, file: Rc<RefCell<File>> },
    Buffer(Rc<RefCell<Cursor<Vec<u8>>>>),
}

impl fmt::Display for Stream {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "#<{}>", match self {
            Self::Stdin(_) => "stdin",
            Self::Stdout(_) => "stdout",
            Self::File { name, .. } => name,
            Self::Buffer(_) => "buffer"
        })
    }
}

impl PartialEq for Stream {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Stdin(_), Self::Stdin(_)) => true,
            (Self::Stdout(_), Self::Stdout(_)) => true,
            (
                Self::File { name: l_name, .. },
                Self::File { name: r_name, .. },
            ) => l_name == r_name,
            _ => false,
        }
    }
}
