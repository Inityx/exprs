pub mod types;

use std::fmt::{Debug, Display};
use self::types::{Cons, Quoted, Symbol, Stryng, Function, Number, Stream};

pub trait FunctionName {
    fn function_name(&self) -> &'static str;
}

impl FunctionName for &'static str {
    fn function_name(&self) -> &'static str { *self }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Expr {
    Nil,
    Cons(Cons),
    Quoted(Quoted),
    Symbol(Symbol),
    Stryng(Stryng),
    Number(Number),
    Function(Function),
    Stream(Stream),
}

impl Display for Expr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Expr::Nil => write!(f, "()"),
            Expr::Cons(cons) => {
                write!(f, "(")?;
                let mut curr = cons;
                while let Expr::Cons(next) = curr.tail_expr() {
                    write!(f, "{} ", curr.head_expr())?;
                    curr = next;
                }
                match curr.tail_expr() {
                    Expr::Cons(_) => unreachable!(),
                    Expr::Nil => write!(f, "{})", curr.head_expr()),
                    _ => write!(f, "{} . {})", curr.head_expr(), curr.tail_expr()),
                }
            },
            Expr::Quoted(quoted) => write!(f, "'{}", quoted.0.as_ref()),
            Expr::Symbol(symbol) => write!(f, "{}", symbol.name()),
            Expr::Stryng(string) => write!(f, "{:?}", string.as_str()),
            Expr::Number(number) => write!(f, "{}", number),
            Expr::Function(proc) => write!(f, "{}", proc),
            Expr::Stream(stream) => write!(f, "{}", stream),
        }
    }
}

impl Expr {
    pub fn new_list<I: IntoIterator<Item=Expr>>(exprs: I) -> Self
    where <I as IntoIterator>::IntoIter: DoubleEndedIterator {
        exprs.into_iter().rfold(Expr::Nil, |cdr, car| Self::Cons(Cons::new(car, cdr)))
    }
}

impl FunctionName for Expr {
    fn function_name(&self) -> &'static str {
        match self {
            Expr::Nil         => "nil",
            Expr::Cons    (_) => "cons",
            Expr::Quoted  (_) => "quoted expression",
            Expr::Symbol  (_) => "symbol",
            Expr::Stryng  (_) => "string",
            Expr::Number  (_) => "number",
            Expr::Function(_) => "function",
            Expr::Stream  (_) => "stream",
        }
    }
}

impl From<Cons>     for Expr { fn from(inner: Cons    ) -> Self { Self::Cons    (inner) } }
impl From<Quoted>   for Expr { fn from(inner: Quoted  ) -> Self { Self::Quoted  (inner) } }
impl From<Symbol>   for Expr { fn from(inner: Symbol  ) -> Self { Self::Symbol  (inner) } }
impl From<Stryng>   for Expr { fn from(inner: Stryng  ) -> Self { Self::Stryng  (inner) } }
impl From<Number>   for Expr { fn from(inner: Number  ) -> Self { Self::Number  (inner) } }
impl From<Function> for Expr { fn from(inner: Function) -> Self { Self::Function(inner) } }
impl From<Stream>   for Expr { fn from(inner: Stream  ) -> Self { Self::Stream  (inner) } }

impl From<[Expr;2]> for Expr {
    fn from([left, right]: [Expr;2]) -> Self {
        Self::Cons(Cons::new(left, right))
    }
}

impl<T> From<Option<T>> for Expr
where Expr: From<T> {
    fn from(inner: Option<T>) -> Self {
        inner.map(Self::from).unwrap_or(Self::Nil)
    }
}

impl From<bool> for Expr {
    fn from(value: bool) -> Self {
        Self::from(value.then_some(Symbol::TRUE))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn format_list() {
        assert_eq!(
            Expr::new_list([
                Symbol::from("+").into(),
                2.0.into(),
                2.5.into(),
            ]).to_string(),
            "(+ 2 2.5)",
        );

        assert_eq!(
            Expr::from(Cons::new(
                Expr::Number(4.0),
                Cons::new(
                    Expr::Number(5.0),
                    Expr::Number(6.0),
                ).into(),
            )).to_string(),
            "(4 5 . 6)",
        );
    }
}
